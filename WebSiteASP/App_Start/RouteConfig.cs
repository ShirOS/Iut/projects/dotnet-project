﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebSiteASP
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "HomePage",
                url: "",
                defaults: new { controller = "Home", action = "Index" }
            );
            routes.MapRoute(
                name: "AddProduct",
                url: "products",
                defaults: new { controller = "Detail", action = "Add" }
            );
            routes.MapRoute(
                name: "DetailProduct",
                url: "detail/products/{id}",
                defaults: new { controller = "Detail", action = "Product" }
            );
            routes.MapRoute(
                name: "DeleteProduct",
                url: "delete/products/{id}",
                defaults: new { controller = "Detail", action = "Delete" }
            );
        }
    }
}
