﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebSiteASP.Models.Product
{
    using Model.Entity.Product;

    public class ProductVM
    {
        public List<Product> List { get; set; }

        public Product Product { get; set; }

        public List<Category> Categories { get; set; }
    }
}