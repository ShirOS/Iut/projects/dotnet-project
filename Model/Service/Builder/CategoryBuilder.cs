﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Service.Builder
{
    using Model.Entity.Product;

    public class CategoryBuilder
    {
        public static Category create(String label)
        {
            Category cateogory = new Category();
            cateogory.Label = label;

            return cateogory;
        }

        public static List<Category> createList()
        {
            List<Category> categories = new List<Category>();
            categories.Add(create("Category 1 Label"));
            categories.Add(create("Category 2 Label"));
            categories.Add(create("Category 3 Label"));
            categories.Add(create("Category 4 Label"));

            return categories;
        }
    }
}
