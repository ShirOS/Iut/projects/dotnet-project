﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Service.Builder
{
    using Model.Entity.Client;
    using Model.Entity.Command;

    public class CommandBuilder
    {
        public static Command create(String observation, Status status, Client client)
        {
            Command command = new Command();
            command.Observation = observation;
            command.Client = client;
            command.ClientId = client.Id;
            command.Status = status;
            command.StatusId = status.Id;

            return command;
        }

        public static List<Command> createList(List<Status> status, List<Client> clients)
        {
            List<Command> commands = new List<Command>();
            commands.Add(create("command 1 observation", status.ElementAt(0), clients.ElementAt(0)));
            commands.Add(create("command 2 observation", status.ElementAt(1), clients.ElementAt(0)));
            commands.Add(create("command 3 observation", status.ElementAt(2), clients.ElementAt(1)));
            commands.Add(create("command 4 observation", status.ElementAt(3), clients.ElementAt(1)));

            return commands;
        }
    }
}
