﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Service.Builder
{
    using Model.Entity.Command;

    public class StatusBuilder
    {
        public static Status create(String label)
        {
            Status status = new Status();
            status.Label = label;

            return status;
        }

        public static List<Status> createList()
        {
            List<Status> status = new List<Status>();
            status.Add(create("Status 1 Label"));
            status.Add(create("Status 2 Label"));
            status.Add(create("Status 3 Label"));
            status.Add(create("Status 4 Label"));

            return status;
        }
    }
}
