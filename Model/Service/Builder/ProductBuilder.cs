﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Service.Builder
{
    using Model.Entity.Product;

    public class ProductBuilder
    {
        public static Product create(String code, String label, String description, int stock, Double price, Category category)
        {
            Product product = new Product();
            product.Code = code;
            product.Label = label;
            product.Description = description;
            product.Stock = stock;
            product.Price = price;
            product.Category = category;
            product.CategoryId = category.Id;

            return product;
        }

        public static List<Product> createList(List<Category> categories)
        {
            List<Product> products = new List<Product>();
            products.Add(create("SHOS01", "ShirOS Product 01", "Web Server", 10, 50.99, categories.ElementAt(0)));
            products.Add(create("SHOS02", "ShirOS Product 02", "Server", 4, 99.99, categories.ElementAt(1)));
            products.Add(create("SHOS03", "ShirOS Product 03", "Keyboard", 25, 85.99, categories.ElementAt(2)));
            products.Add(create("SHOS04", "ShirOS Product 04", "Mouse", 18, 48.99, categories.ElementAt(3)));

            return products;
        }
    }
}
