﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Repository.Command
{
    using Entity.Command;
    using Service.Builder;

    public class CommandRepository : Repository
    {
        public Command Add(Command command)
        {
            this.Context.Command.Add(command);
            this.Context.SaveChanges();

            return command;
        }

        public List<Command> AddObjects(List<Command> commands)
        {
            foreach (Command element in commands)
            {
                this.Add(element);
            }

            return commands;
        }

        public List<Command> List()
        {
            return this.Context.Command.ToList();
            //return CommandBuilder.createList();
        }
    }
}
