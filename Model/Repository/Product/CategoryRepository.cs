﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Repository.Product
{
    using Entity.Product;

    public class CategoryRepository : Repository
    {
        public Category Add(Category category)
        {
            this.Context.Category.Add(category);
            this.Context.SaveChanges();

            return category;
        }

        public List<Category> AddObjects(List<Category> categories)
        {
            foreach (Category element in categories)
            {
                this.Add(element);
            }

            return categories;
        }

        public List<Category> List()
        {
            return this.Context.Category.ToList();
        }

        public Category GetCategory(int id)
        {
            return this.Context.Category.Where(category => category.Id == id).First();
        }
    }
}
